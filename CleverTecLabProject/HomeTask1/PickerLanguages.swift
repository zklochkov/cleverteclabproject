//
//  PickerLanguages.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 11.09.21.
//

import Foundation

//enum PickerLanguages: String, CaseIterable, Identifiable {
//    case ru = "Russian"
//    case en = "English"
//    case be = "Belarusian"
//
//    var id: String {self.rawValue}
//    var description: String { return NSLocalizedString(self.rawValue, bundle: Bundle.localizedBundle(), comment: "")}
//
//    static func getLang(name: PickerLanguages) -> String {
//        switch name {
//        case ru:
//            return "ru"
//        case en:
//            return "en"
//        case be:
//            return "be"
//        }
//    }
//}

enum PickerLanguages: Int, CaseIterable, Identifiable {
    var id: Int {self.rawValue}
    
    case ru = 1, en, be
    
    //    var description: String { return NSLocalizedString(self.rawValue, bundle: Bundle.localizedBundle(), comment: "")}
    func getCurrentLang() -> String {
        return PickerLanguages.getLangByNumber(name: rawValue)
    }
    var label: String {
        switch self {
        case .ru: return NSLocalizedString("Russian", bundle: Bundle.localizedBundle(), comment: "")
        case .en: return NSLocalizedString("English", bundle: Bundle.localizedBundle(), comment: "")
        case .be: return NSLocalizedString("Belarusian", bundle: Bundle.localizedBundle(), comment: "")
        }
    }
    
    static func getLang(name: PickerLanguages) -> String {
        switch name {
        case ru:
            return "ru"
        case en:
            return "en"
        case be:
            return "be"
        }
    }
    
    static func getLangByNumber(name: Int) -> String {
        switch name {
        case 1:
            return "ru"
        case 2:
            return "en"
        case 3:
            return "be"
        default:
            return "undefined"
        }
    }
    static func getLangNumber(name: String) -> Int {
        switch name {
        case "ru":
            return 1
        case "en":
            return 2
        case "be":
            return 3
        default:
            return 0
        }
    }
    
}
