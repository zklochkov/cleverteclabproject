//
//  ViewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 6.09.21.
//

import UIKit

class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    private lazy var imageView: UIImageView = {
        let theImageView = UIImageView()
        theImageView.image = UIImage(named: "baseline_bug_report_black_48pt")
        theImageView.translatesAutoresizingMaskIntoConstraints = false
        
        return theImageView
    }()
    
    private lazy var labelGreetingText: UILabel = {
        let label = UILabel()
        label.text = "greeting-title".localized()
        label.textColor = .label
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var languagePicker: UIPickerView = {
        let picker = UIPickerView()
        picker.translatesAutoresizingMaskIntoConstraints = false
        
        return picker
    }()
    
    private lazy var themeAutoButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemFill
        button.tintColor = .label
        button.tag = 0
        button.setTitleColor(UIColor.init(white: 1, alpha: 0.3), for: .highlighted)
        button.layer.cornerRadius = 5
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var themeDarkButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemGray
        button.tintColor = .label
        button.tag = 1
        button.setTitleColor(UIColor.init(white: 1, alpha: 0.3), for: .highlighted)
        button.layer.cornerRadius = 5
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var themeLightButton: UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .systemGray
        button.tintColor = .label
        button.tag = 2
        button.layer.cornerRadius = 5
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        
        return button
    }()
    
    @objc func buttonAction(sender: UIButton!) {
        
        switch sender.tag {
        case 0:
            self.overrideUserInterfaceStyle = .unspecified
            self.view.backgroundColor = .systemBackground
            themeAutoButton.backgroundColor = .systemFill
            themeDarkButton.backgroundColor = .systemGray
            themeLightButton.backgroundColor = .systemGray
        case 1:
            self.overrideUserInterfaceStyle = .dark
            self.view.backgroundColor = .systemBackground
            view.backgroundColor = .systemBackground
            themeAutoButton.backgroundColor = .systemGray
            themeDarkButton.backgroundColor = .systemFill
            themeLightButton.backgroundColor = .systemGray
        case 2:
            self.overrideUserInterfaceStyle = .light
            self.view.backgroundColor = .systemBackground
            self.themeAutoButton.backgroundColor = .systemGray
            self.themeDarkButton.backgroundColor = .systemGray
            self.themeLightButton.backgroundColor = .systemFill
        default:
            print("Button tapped")
        }
        sender.showAnimation {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Bundle.setSavedDefaultLanguage()
        self.view.backgroundColor = .systemBackground
        self.view.addSubview(imageView)
        self.labelGreetingText.text = "greeting-title".localized()
        self.view.addSubview(labelGreetingText)
        self.languagePicker.delegate = self
        self.languagePicker.dataSource = self
        self.languagePicker.selectRow(PickerLanguages.getLangNumber(name: Bundle.getSavedDefaultLanguage()) - 1, inComponent: 0, animated: true)
        self.view.addSubview(languagePicker)
        self.themeAutoButton.setTitle("theme-auto-button".localized(), for: .normal)
        self.view.addSubview(themeAutoButton)
        self.themeDarkButton.setTitle("theme-dark-button".localized(), for: .normal)
        self.view.addSubview(themeDarkButton)
        self.themeLightButton.setTitle("theme-light-button".localized(), for: .normal)
        self.view.addSubview(themeLightButton)
        
        constraintsInit()
    }
    
    func constraintsInit() {
        NSLayoutConstraint.activate([
            
            imageView.widthAnchor.constraint(equalToConstant: 100),
            imageView.heightAnchor.constraint(equalToConstant: 100),
            
            imageView.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            imageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            labelGreetingText.topAnchor.constraint(equalTo: imageView.bottomAnchor),
            labelGreetingText.centerXAnchor.constraint(equalTo: imageView.centerXAnchor),
            
            languagePicker.topAnchor.constraint(equalTo: labelGreetingText.bottomAnchor),
            languagePicker.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            themeAutoButton.topAnchor.constraint(equalTo: languagePicker.bottomAnchor, constant: 8),
            themeAutoButton.centerXAnchor.constraint(equalTo: languagePicker.centerXAnchor),
            
            themeAutoButton.widthAnchor.constraint(equalToConstant: self.languagePicker.frame.size.width),
            themeAutoButton.heightAnchor.constraint(equalToConstant: 36),
            
            themeDarkButton.topAnchor.constraint(equalTo: themeAutoButton.bottomAnchor, constant: 8),
            themeDarkButton.centerXAnchor.constraint(equalTo: themeAutoButton.centerXAnchor),
            
            themeDarkButton.widthAnchor.constraint(equalToConstant: self.languagePicker.frame.size.width),
            themeDarkButton.heightAnchor.constraint(equalToConstant: 36),
            
            themeLightButton.topAnchor.constraint(equalTo: themeDarkButton.bottomAnchor, constant: 8),
            themeLightButton.centerXAnchor.constraint(equalTo: themeAutoButton.centerXAnchor),
            
            themeLightButton.widthAnchor.constraint(equalToConstant: self.languagePicker.frame.size.width),
            themeLightButton.heightAnchor.constraint(equalToConstant: 36)
        ])
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let titleData = PickerLanguages.allCases[row].label
        let myTitle = NSAttributedString(string: titleData.description, attributes: [NSAttributedString.Key.font:UIFont(name: "Georgia", size: 15.0)!,NSAttributedString.Key.foregroundColor:UIColor.label])
        return myTitle
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return PickerLanguages.allCases.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return PickerLanguages.allCases[row].label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        print(PickerLanguages.allCases[row])
        Bundle.setLanguage(lang: PickerLanguages.getLang(name: PickerLanguages.allCases[row]))
        viewDidLoad()
    }
}
