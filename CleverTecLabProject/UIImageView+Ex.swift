//
//  UIImageView+Ex.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 20.09.21.
//

import UIKit

extension UIImageView {

    func makeRounded() {

        self.layer.borderWidth = 1
        self.layer.masksToBounds = false
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
    }
}
