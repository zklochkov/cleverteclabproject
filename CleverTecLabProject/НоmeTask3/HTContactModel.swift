//
//  ContactModel.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 28.09.21.
//

import Foundation
import ContactsUI

class HTContactModel {
    let firstName: String
    let lastName: String
    let workEmail: String
    let phoneNumber: String
    var phoneNumberField: (CNLabeledValue<CNPhoneNumber>)?
    
    init(firstName: String, lastName: String, workEmail: String, phoneNumber: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.workEmail = workEmail
        self.phoneNumber = phoneNumber
    }
}

extension HTContactModel: Equatable {
    static func ==(lhs: HTContactModel, rhs: HTContactModel) -> Bool {
        return lhs.firstName == rhs.firstName &&
            lhs.lastName == rhs.lastName &&
            lhs.workEmail == rhs.workEmail &&
            lhs.phoneNumber == rhs.phoneNumber
    }
}

extension HTContactModel {
    var contactValue: CNContact {
        let contact = CNMutableContact()
        contact.givenName = firstName
        contact.familyName = lastName
        contact.emailAddresses = [CNLabeledValue(label: CNLabelWork, value: workEmail as NSString)]
        if let phoneNumberField = phoneNumberField {
            contact.phoneNumbers.append(phoneNumberField)
        }
        return contact
    }
    
    convenience init?(contact: CNContact) {
        guard let email = contact.emailAddresses.first else { return nil }
        let firstName = contact.givenName
        let lastName = contact.familyName
        let workEmail = email.value as String
        self.init(firstName:firstName, lastName:lastName,workEmail:workEmail,phoneNumber:contact.phoneNumbers.first?.value.stringValue ?? "")
    }
}
