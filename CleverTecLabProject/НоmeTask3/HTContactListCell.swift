//
//  ContactListCell.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 27.09.21.
//

import SwiftUI

class HTContactListCell: UITableViewCell {
    
    var cell: HTContactModel? {
        didSet{
            firstNameCell.text = cell?.firstName
            lastNameCell.text = cell?.lastName
            phoneNumberCell.text = cell?.phoneNumber
            emailCell.text = cell?.workEmail
        }
    }
    
    private let firstNameCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .left
        
        return label
    }()
    
    private let lastNameCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .thin)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    private let phoneNumberCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    private let emailCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .thin)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    
    private let imageCell: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "baseline_bug_report_black_48pt")
        
        imageView.layer.cornerRadius = 24
        imageView.layer.backgroundColor = UIColor.lightGray.cgColor
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(imageCell)
        addSubview(firstNameCell)
        addSubview(lastNameCell)
        addSubview(phoneNumberCell)
        addSubview(emailCell)
        
        imageCell.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 48, height: 48, enableInsets: false)
        
        firstNameCell.anchor(top: topAnchor, left: imageCell.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 3, height: 0, enableInsets: false)
        
        lastNameCell.anchor(top: firstNameCell.bottomAnchor, left: imageCell.rightAnchor, bottom: nil, right: nil, paddingTop: 4, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 3, height: 0, enableInsets: false)
        
        phoneNumberCell.anchor(top: topAnchor, left: firstNameCell.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width - frame.size.width / 3, height: 0, enableInsets: false)
        
        emailCell.anchor(top: phoneNumberCell.bottomAnchor, left: lastNameCell.rightAnchor, bottom: nil, right: nil, paddingTop: 4, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width - frame.size.width / 3, height: 0, enableInsets: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
