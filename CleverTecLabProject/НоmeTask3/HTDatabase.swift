//
//  HTDatabase.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 27.09.21.
//

import Foundation
import CoreData

typealias HTDB = HTDatabase

class HTDatabase {
    
    static let sh = HTDatabase()
    
    // MARK: - properties
    
    private static let databaseContainerName = "ContactsDB"
    
    private let documentsDirectory: URL
    
    // MARK: - core data
    
    private lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: HTDB.databaseContainerName)
        container.loadPersistentStores { (description, error) in
            if let error = error {
                Swift.debugPrint(error.localizedDescription)
            }
            Swift.debugPrint("Store description: \(description)")
        }
        
        return container
    }()
    
    private var context: NSManagedObjectContext {
        self.persistentContainer.viewContext
    }
    
    // MARK: - initialization
    
    private init() {
        let docDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        if docDirectory.count > 0 {
            self.documentsDirectory = docDirectory[0]
        } else {
            fatalError("Directory doesn't exist")
        }
    }
    
    // MARK: - core data functions
    
    func saveContext() {
        if self.context.hasChanges {
            Swift.debugPrint("Context inserted objects: \(self.context.insertedObjects)")
            Swift.debugPrint("Context deleted objects: \(self.context.deletedObjects)")
            
            do {
                try self.context.save()
                Swift.debugPrint("Context was saved")
            } catch {
                let nsError = error as NSError
                Swift.debugPrint("Couldn't saved data. Reason: \(nsError.localizedDescription) \(nsError.userInfo)")
            }
        }
    }
    
    func save(contact: HTContactModel) {
        self.getCoreDataObject(from: contact)
        self.saveContext()
    }
    
    // TODO: - add core data functions
    
    func loadMovies() -> [HTContactModel] {
        // 1
        let fetchRequest: NSFetchRequest<ContactEntity> = ContactEntity.fetchRequest()
        //        let sortDescriptor = NSSortDescriptor(key: #keyPath(Movie.title), ascending: true)
        //        fetchRequest.sortDescriptors = [sortDescriptor]
        
        do {
            // 2
            let contacts = try self.context.fetch(fetchRequest)
            // 3
            return contacts.map { $0.getContact() }
        } catch {
            Swift.debugPrint(error.localizedDescription)
            return []
        }
    }
    
    //    func delete(_ movie: MWMovie) {
    //        let fetchRequest: NSFetchRequest<Movie> = Movie.fetchRequest()
    //        fetchRequest.predicate = NSPredicate(format: "id == %d", movie.id)
    //
    //        do {
    //            let results = try self.context.fetch(fetchRequest)
    //            results.forEach { self.context.delete($0) }
    //        } catch let error as NSError {
    //            Swift.debugPrint("Couldn't remove data. \(error), \(error.userInfo)")
    //        }
    //
    //        self.saveContext()
    //    }
    
    @discardableResult
    private func getCoreDataObject(from contact: HTContactModel) -> ContactEntity {
        
        let coreDataContact = ContactEntity(context: self.context)
        coreDataContact.set(contact)
        
        return coreDataContact
    }
}
