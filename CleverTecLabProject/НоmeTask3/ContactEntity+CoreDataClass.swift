//
//  ContactEntity+CoreDataClass.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 27.09.21.
//
//

import Foundation
import CoreData

@objc(ContactEntity)
public class ContactEntity: NSManagedObject {
    func set(_ contact: HTContactModel) {
        self.firstNameContact = contact.firstName
        self.lastNameContact = contact.lastName
        self.emailContact = contact.workEmail
        self.phoneNumberContact = contact.phoneNumber
    }
    
    func getContact() -> HTContactModel {
        return HTContactModel(firstName: self.firstNameContact ?? "", lastName: self.lastNameContact ?? "", workEmail: self.emailContact ?? "", phoneNumber: self.phoneNumberContact ?? "")
    }
}
