//
//  ContactEntity+CoreDataProperties.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 27.09.21.
//
//

import Foundation
import CoreData


extension ContactEntity {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<ContactEntity> {
        return NSFetchRequest<ContactEntity>(entityName: "ContactEntity")
    }
    
    @NSManaged public var emailContact: String?
    @NSManaged public var firstNameContact: String?
    @NSManaged public var lastNameContact: String?
    @NSManaged public var phoneNumberContact: String?
    
}

extension ContactEntity : Identifiable {
    
}
