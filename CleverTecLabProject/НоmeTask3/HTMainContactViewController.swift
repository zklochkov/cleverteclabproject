//
//  HTMainContactViewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 23.09.21.
//

import SwiftUI
import ContactsUI
import CoreData


class HTMainContactViewController: UIViewController {
    
    var contactsList: [HTContact] = []
    let defaults = UserDefaults.standard
    
    private lazy var chooseContactButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemFill
        button.tintColor = .label
        button.tag = 0
        button.setTitleColor(UIColor.init(white: 1, alpha: 0.3), for: .highlighted)
        button.layer.cornerRadius = 20
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.addTarget(self, action: #selector(chooseContactAction), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var openContactListButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemFill
        button.tintColor = .label
        button.tag = 0
        button.setTitleColor(UIColor.init(white: 1, alpha: 0.3), for: .highlighted)
        button.layer.cornerRadius = 20
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.addTarget(self, action: #selector(openContactListAction), for: .touchUpInside)
        
        return button
    }()
    
    private lazy var viewSaveContactButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.backgroundColor = .systemFill
        button.tintColor = .label
        button.tag = 0
        button.setTitleColor(UIColor.init(white: 1, alpha: 0.3), for: .highlighted)
        button.layer.cornerRadius = 20
        button.contentEdgeInsets = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        button.addTarget(self, action: #selector(viewSaveContactAction), for: .touchUpInside)
        
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.chooseContactButton.setTitle("choose-contact".localized(), for: .normal)
        self.view.addSubview(chooseContactButton)
        self.openContactListButton.setTitle("open-contact-list".localized(), for: .normal)
        self.view.addSubview(openContactListButton)
        self.viewSaveContactButton.setTitle("show-saved-phone-number".localized(), for: .normal)
        self.view.addSubview(viewSaveContactButton)
        constraintsInit()
    }
    
    func constraintsInit() {
        NSLayoutConstraint.activate([
            chooseContactButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            chooseContactButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            openContactListButton.topAnchor.constraint(equalTo: chooseContactButton.bottomAnchor, constant: 24),
            openContactListButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            viewSaveContactButton.topAnchor.constraint(equalTo: openContactListButton.bottomAnchor, constant: 24),
            viewSaveContactButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
    
    @objc func viewSaveContactAction() {
        if let phoneNumber = defaults.string(forKey: "PhoneNumber") {
            showAlert(title: "PhoneNumber", message: phoneNumber)
        }
        else {
            showAlert(title: "PhoneNumber", message: "Phone number not save!")
        }
    }
    
    @objc func openContactListAction() {
        let nextVC = HTContactsListViewController()
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @objc func chooseContactAction() {
        if isAccessPermitionContact() {
            let contactPicker = CNContactPickerViewController()
            contactPicker.delegate = self
            contactPicker.predicateForEnablingContact = NSPredicate(format: "phoneNumbers.@count > 0")
            present(contactPicker, animated: true, completion: nil)
        }
        else {
            showAlert(title: "Access granted", message: "Tap again to open contact list")
        }
    }
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default))
        present(alert, animated: true, completion: nil)
    }
    
    func requestAuthorizationPermission() -> Bool {
        var isPermitionContactAccess:Bool = false
        let store = CNContactStore()
        store.requestAccess(for: .contacts) { success, error in
            if let error = error {
                self.showAlert(title: "Not Determined", message: "Not authorized to access contacts. Error = \(String(describing: error))")
            }
            
            if success {
                isPermitionContactAccess = success
                print("Access granted")
            }
        }
        return isPermitionContactAccess
    }
    
    func isAccessPermitionContact() -> Bool {
        var isPermitionContactAccess:Bool = false
        
        let authStatus = CNContactStore.authorizationStatus(for: .contacts)
        switch authStatus {
        case .notDetermined:
            isPermitionContactAccess = requestAuthorizationPermission()
        case .restricted:
            showAlert(title: "Restricted", message: "User cannot grant permission, e.g. parental controls in force.")
        case .denied:
            showAlert(title: "User has explicitly denied permission.", message: "They have to grant it via Preferences app if they change their mind.")
        case .authorized:
            isPermitionContactAccess = requestAuthorizationPermission()
        @unknown default:
            print("unknown case")
        }
        return isPermitionContactAccess
    }
}

extension HTMainContactViewController: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contacts: [CNContact]) {
        let newContacts = contacts.compactMap { HTContactModel(contact: $0) }
        for contact in newContacts {
            HTDB.sh.save(contact: contact)
        }
        showAlert(title: "Success", message: "Сохранение успешно")
    }
}

