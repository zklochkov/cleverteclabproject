//
//  ContactsListVIewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 27.09.21.
//

import UIKit
import CoreData

class HTContactsListViewController: UITableViewController {
    
    let cellId = "cellId"
    var contacts : [HTContactModel] = [HTContactModel]()
    let defaults = UserDefaults.standard
    
    private let firstNameCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .left
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.addSubview(firstNameCell)
        createTableCell()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HTContactListCell.self, forCellReuseIdentifier: cellId)
        
        constraintsInit()
    }
    
    func constraintsInit() {
        NSLayoutConstraint.activate([
            
            firstNameCell.bottomAnchor.constraint(equalTo: tableView.topAnchor, constant: 100),
            firstNameCell.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
        ])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? HTContactListCell else { return HTContactListCell() }
        if indexPath.row < contacts.count {
            let currentLastItem = contacts[indexPath.row]
            cell.cell = currentLastItem
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        firstNameCell.text = contacts[indexPath.row].firstName
        defaults.set(contacts[indexPath.row].phoneNumber, forKey: "PhoneNumber")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func createTableCell() {
        for contact in HTDB.sh.loadMovies() {
            contacts.append(HTContactModel(firstName: contact.firstName, lastName: contact.lastName, workEmail: contact.workEmail, phoneNumber: contact.phoneNumber))
        }
    }
}
