//
//  MainViewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 3.10.21.
//

import UIKit
import MapKit

class MainViewController: UIViewController, MKMapViewDelegate {
    
    private var networkManager: NetworkManager!
    private var langCenter = 52.42505
    private var longCenter = 31.01407
    private let distanceSpan: CLLocationDegrees = 3000
    private let annotation = MKPointAnnotation()
    private var location: CLLocationCoordinate2D = CLLocationCoordinate2D()
    
    private lazy var mapView: MKMapView = {
        var mp = MKMapView()
        mp.mapType = MKMapType.standard
        mp.isZoomEnabled = true
        mp.isScrollEnabled = true
        
        return mp
    }()
    
    init(networkManager: NetworkManager) {
        super.init(nibName: nil, bundle: nil)
        self.networkManager = networkManager
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftMargin:CGFloat = 10
        let topMargin:CGFloat = 10
        
        mapView.frame = CGRect(x: leftMargin, y: topMargin, width: view.frame.size.width, height: view.frame.size.height)
        mapView.center = view.center
        mapView.delegate = self
        
        view.addSubview(mapView)
        DispatchQueue.main.async {
            self.networkManager.getAllATM() {
                atms, error in
                if let error = error {
                    print(error)
                }
                if let atms = atms {
                    self.addATMPoints(arrayPoints: atms)
                    print(atms)
                }
            }
        }
        view.backgroundColor = .green
    }
    
    var atmPositionArr: [ATM] = [ATM]()
    
    func getATM() {
        guard let url = URL(string: "https://belarusbank.by/api/atm") else { return }
        let session = URLSession.shared
        session.dataTask(with: url) {(data, response, error) in
            if let response = response {
                print(response)
            }
            
            guard let data = data else {return}
            print(data)
            
            do{
                let json = try JSONDecoder().decode([ATM].self, from: data)
                DispatchQueue.main.async {
                    self.addATMPoints(arrayPoints: json)
                }
                print(json)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    func addATMPoints(arrayPoints: [ATM]){
        for atm in arrayPoints {
            guard let lang = Double(atm.gps_x) else { return }
            guard let long = Double(atm.gps_y) else { return }
            self.addAnnotation(lang: lang.rounded(toPlaces: 5), long: long.rounded(toPlaces: 5), title: "")
        }
    }
    
    func setCenterMap(lang: Double, long: Double){
        location = CLLocationCoordinate2DMake(lang, long)
        let coordinateRegion = MKCoordinateRegion(center: location, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func addAnnotation(lang: CLLocationDegrees, long: CLLocationDegrees, title: String) {
        location = CLLocationCoordinate2DMake(lang, long)
        let pin = MKPlacemark(coordinate: location)
        let coordinateRegion = MKCoordinateRegion(center: pin.coordinate, latitudinalMeters: distanceSpan, longitudinalMeters: distanceSpan)
        mapView.setRegion(coordinateRegion, animated: true)
        
        //        annotation.coordinate = location
        //        annotation.title = title
        mapView.addAnnotation(pin)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

