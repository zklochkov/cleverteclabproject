//
//  HTATMEndPoint.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 3.10.21.
//

import Foundation

enum NetworkEnvironment {
    case atm
}

public enum ATMApi {
    case atm
    case city(city:String)
}

extension ATMApi: EndPointType {
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .atm: return "https://belarusbank.by/api"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured") }
        print(url)
        return url
    }
    
    var path: String {
        switch self {
        case .city(let city):
            return "\(city)"
        case .atm:
            return "atm"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .city(let city):
            return .requestParameters(bodyParameters: nil, urlParameters: ["city": city])
        default:
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}
