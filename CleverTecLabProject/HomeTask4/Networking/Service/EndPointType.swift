//
//  EndPointType.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 2.10.21.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
    
}
