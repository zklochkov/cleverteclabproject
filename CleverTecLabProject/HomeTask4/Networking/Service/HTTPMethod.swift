//
//  HTTPMethod.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 2.10.21.
//

import Foundation

public enum HTTPMethod : String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case patch = "PATCH"
    case delete = "DELETE"
}
