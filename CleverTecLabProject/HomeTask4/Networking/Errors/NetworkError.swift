//
//  NetworkError.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 2.10.21.
//

import Foundation

public enum NetworkError : String, Error {
    case parametersNil = "Parameters were nil"
    case encodingFailed = "Parameters encoding failed"
    case missingURL = "URL is nil"
}
