//
//  ParameterEncoding.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 2.10.21.
//

import Foundation

public typealias Parameters = [String: Any]

public protocol ParameterEncoding {
    static func encode(urlRequest: inout URLRequest, with parameters: Parameters) throws
}
