//
//  Double+Ex.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 11.10.21.
//

import Foundation

extension Double {
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
