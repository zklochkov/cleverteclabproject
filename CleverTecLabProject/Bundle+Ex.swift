//
//  Bundle+Ex.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 11.09.21.
//

import Foundation

extension Bundle {
    private static var bundle: Bundle!
    
    public static func localizedBundle() -> Bundle! {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "AppleLanguages") ?? "ru"
            let path = Bundle.main.path(forResource: appLang.description, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
        
        return bundle;
    }
    
    public static func setLanguage(lang: String) {
        UserDefaults.standard.set(lang, forKey: "AppleLanguages")
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        bundle = Bundle(path: path!)
    }
    
    public static func setSavedDefaultLanguage() {
        if bundle == nil {
            let appLang = UserDefaults.standard.string(forKey: "AppleLanguages") ?? "ru"
            let path = Bundle.main.path(forResource: appLang.description, ofType: "lproj")
            bundle = Bundle(path: path!)
        }
    }
    
    public static func getSavedDefaultLanguage() -> String {
        guard let appLang = UserDefaults.standard.string(forKey: "AppleLanguages") else {
            return "ru"
        }
        return appLang
    }
}
