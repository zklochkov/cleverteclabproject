//
//  String+Ex.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 11.09.21.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.localizedBundle(), value: "", comment: "")
    }
}
