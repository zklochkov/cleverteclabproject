//
//  TableCellModel.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 19.09.21.
//

import UIKit

struct HTTableCellModel {
    var titleCell: String
    var descriptionCell: String
    var imageCell: UIImage
}
