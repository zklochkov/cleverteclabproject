//
//  HTTableCell.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 19.09.21.
//

import UIKit

class HTTableCell: UITableViewCell {
    
    var cell: HTTableCellModel? {
        didSet{
            titleCell.text = cell?.titleCell
            descriptionCell.text = cell?.descriptionCell
            imageCell.image = cell?.imageCell
        }
    }
    
    private let titleCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.textAlignment = .left
        
        return label
    }()
    
    private let descriptionCell: UILabel = {
        let label = UILabel()
        label.textColor = .label
        label.font = UIFont.systemFont(ofSize: 14, weight: .thin)
        label.textAlignment = .left
        label.numberOfLines = 0
        
        return label
    }()
    
    
    private let imageCell: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "baseline_bug_report_black_48pt")
        
        imageView.layer.cornerRadius = 24
        imageView.layer.backgroundColor = UIColor.lightGray.cgColor
        imageView.clipsToBounds = true
        
        return imageView
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(titleCell)
        addSubview(descriptionCell)
        addSubview(imageCell)
        
        imageCell.anchor(top: topAnchor, left: leftAnchor, bottom: bottomAnchor, right: nil, paddingTop: 5, paddingLeft: 5, paddingBottom: 5, paddingRight: 0, width: 48, height: 48, enableInsets: false)

        titleCell.anchor(top: topAnchor, left: imageCell.rightAnchor, bottom: nil, right: nil, paddingTop: 10, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)

        descriptionCell.anchor(top: titleCell.bottomAnchor, left: imageCell.rightAnchor, bottom: nil, right: nil, paddingTop: 4, paddingLeft: 10, paddingBottom: 0, paddingRight: 0, width: frame.size.width / 2, height: 0, enableInsets: false)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
