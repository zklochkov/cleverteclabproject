//
//  MainTableViewController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 14.09.21.
//

import SwiftUI

class HTMainTableViewController: UITableViewController {
    
    let cellId = "cellId"
    var products : [HTTableCellModel] = [HTTableCellModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createTableCell()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(HTTableCell.self, forCellReuseIdentifier: cellId)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as? HTTableCell else { return HTTableCell() }
        if indexPath.row < products.count {
            let currentLastItem = products[indexPath.row]
            cell.cell = currentLastItem
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let newVC = HTTableDetailsViewController()
        newVC.imageCell = products[indexPath.row].imageCell
        newVC.titleCell = products[indexPath.row].titleCell
        newVC.descriptionCell = products[indexPath.row].descriptionCell
        tableView.deselectRow(at: indexPath, animated: true)
        self.navigationController?.pushViewController(newVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func createTableCell() {
        for i in 0...1000 {
            products.append(HTTableCellModel(titleCell: "Title\(i)", descriptionCell: "Description\(i)", imageCell: #imageLiteral(resourceName: "baseline_bug_report_black_48pt")))
        }
    }
}
