//
//  NameController.swift
//  CleverTecLabProject
//
//  Created by Zakhar Klochkov on 20.09.21.
//

import UIKit

class HTTableDetailsViewController: UIViewController {
    
    var imageCell: UIImage?
    var titleCell: String?
    var descriptionCell: String?
    
    private lazy var imageDetail: UIImageView = {
        let theImageView = UIImageView()
        theImageView.translatesAutoresizingMaskIntoConstraints = false
        
        return theImageView
    }()
    
    private lazy var titleDetail: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private lazy var descriptionDetail: UILabel = {
        let label = UILabel()
        label.textColor = .black
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Title"
        self.view.backgroundColor = .white
        
        if let titleCell = self.titleCell {
            titleDetail.text = titleCell
        }
        
        if let descriptionCell = self.descriptionCell {
            descriptionDetail.text = descriptionCell
        }
        
        if let imageCell = self.imageCell {
            imageDetail.image = imageCell
        }
        
        view.addSubview(titleDetail)
        view.addSubview(descriptionDetail)
        view.addSubview(imageDetail)
        constraintsInit()
    }
    
    func constraintsInit() {
        NSLayoutConstraint.activate([
            
            imageDetail.widthAnchor.constraint(equalToConstant: 100),
            imageDetail.heightAnchor.constraint(equalToConstant: 100),
            
            imageDetail.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
            imageDetail.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            titleDetail.topAnchor.constraint(equalTo: imageDetail.bottomAnchor, constant: 10),
            titleDetail.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            
            descriptionDetail.topAnchor.constraint(equalTo: titleDetail.bottomAnchor, constant: 10),
            descriptionDetail.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }
}
